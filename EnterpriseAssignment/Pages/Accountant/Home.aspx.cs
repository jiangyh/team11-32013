﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnterpriseAssignment
{
    public partial class AccountantHome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ButtonGenerateReport_Click(object sender, EventArgs e)
        {
            
            if (RadioButtonTotalCosts.Checked)
                Response.Redirect("ReportTotalCosts.aspx");
            if (RadioButtonAverageCosts.Checked)
                Response.Redirect("ReportAvCosts.aspx");
            if (RadioButtonDistrictCosts.Checked)
                Response.Redirect("ReportByDistrict.aspx");
            if (RadioButtonMonthCostDistrict.Checked)
                Response.Redirect("ReportMonthCostDistrict.aspx");
        }

        protected void LinkButtonChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangePassword.aspx");
        }

        protected void UsersGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedUserName = UsersGridView.SelectedRow.Cells[1].Text;
            Response.Redirect("ChangeDistrict.aspx?userName=" + selectedUserName);
        }
    }
}