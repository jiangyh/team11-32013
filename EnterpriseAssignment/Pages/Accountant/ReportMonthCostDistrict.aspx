﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportMonthCostDistrict.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.AccountantReportMonthCostDistrict" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
        Monthly Costs for District.<br />
        Please choose district<br />
        <asp:DropDownList ID="DistrictDropDownList" runat="server" AutoPostBack="true">
            <asp:ListItem>UrbanIndonesia</asp:ListItem>
            <asp:ListItem>RuralIndonesia</asp:ListItem>
            <asp:ListItem>UrbanPapuaNewGuinea</asp:ListItem>
            <asp:ListItem>RuralPapuaNewGuinea</asp:ListItem>
            <asp:ListItem>Sydney</asp:ListItem>
            <asp:ListItem>RuralNewSouthWales</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:GridView ID="AverageCostsGridView" runat="server" DataSourceID="SqlDataSource" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Year" HeaderText="Year" SortExpression="Year" ReadOnly="True" />
                <asp:BoundField DataField="Month" HeaderText="Month" ReadOnly="True" SortExpression="Month" />
                <asp:BoundField DataField="MonthlyLabourHours" HeaderText="MonthlyLabourHours" ReadOnly="True" SortExpression="MonthlyLabourHours" />
                <asp:BoundField DataField="MonthlyCost" HeaderText="MonthlyCost" ReadOnly="True" SortExpression="MonthlyCost" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
            SelectCommand="select year(PerformedDate) as Year, month(PerformedDate) as Month, sum(Intervention.LabourRequired) as MonthlyLabourHours,sum(Intervention.Cost) as MonthlyCost
            from Intervention
            left join Client on Intervention.ClientId=Client.ClientId
            where State='Completed' and (DistrictName=@DistrictName)
            group by year(PerformedDate), month(PerformedDate);"
            >
            <SelectParameters>
                <asp:ControlParameter Name="DistrictName" ControlID="DistrictDropDownList" Type="String"  PropertyName="SelectedValue"/>
            </SelectParameters>
        </asp:SqlDataSource>
        <br />

    </div>

</asp:Content>