﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangeDistrict.aspx.cs" Inherits="EnterpriseAssignment.Pages.Accountant.ChangeDistrict" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">


    <p>
        <br />
                    <asp:DetailsView ID="DetailsViewClient" runat="server" AutoGenerateRows="False" DataSourceID="UserDetailsDataSource">
                        <Fields>
                            <asp:BoundField DataField="LoginUserName" HeaderText="LoginUserName" SortExpression="LoginUserName" />
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                            <asp:BoundField DataField="ResponsibleDistrict" HeaderText="ResponsibleDistrict" SortExpression="ResponsibleDistrict" />
                            <asp:BoundField DataField="ApprovableLabour" HeaderText="ApprovableLabour" SortExpression="ApprovableLabour" />
                            <asp:BoundField DataField="ApprovableCost" HeaderText="ApprovableCost" SortExpression="ApprovableCost" />
                        </Fields>
                    </asp:DetailsView>
                    <asp:ObjectDataSource ID="UserDetailsDataSource" runat="server" SelectMethod="GetUserInfoByUserName" TypeName="EnterpriseAssignment.BusinessLogic.UserLogic">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="userName" QueryStringField="userName" Type="String" />
                        </SelectParameters>
        </asp:ObjectDataSource>
    </p>
    <p>
        <asp:DropDownList ID="DistrictDropDownList" runat="server" AutoPostBack="true">
            <asp:ListItem>UrbanIndonesia</asp:ListItem>
            <asp:ListItem>RuralIndonesia</asp:ListItem>
            <asp:ListItem>UrbanPapuaNewGuinea</asp:ListItem>
            <asp:ListItem>RuralPapuaNewGuinea</asp:ListItem>
            <asp:ListItem>Sydney</asp:ListItem>
            <asp:ListItem>RuralNewSouthWales</asp:ListItem>
        </asp:DropDownList>
    </p>
    <p>
        <asp:Button ID="ChangeButton" runat="server" Text="Change" OnClick="ChangeButton_Click"/>
    </p>
    <p>
        <asp:Button ID="BackButton" runat="server" OnClick="BackButton_Click" Text="GoBack" />
    </p>
    <p>
        &nbsp;</p>


</asp:Content>
