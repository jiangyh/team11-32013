﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.BusinessLogic;
using EnterpriseAssignment.Models;

namespace EnterpriseAssignment.Pages.Accountant
{
    public partial class ChangeDistrict : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["userName"] == null)
                Response.Redirect("Home.aspx");
        }

        protected void BackButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

        protected void ChangeButton_Click(object sender, EventArgs e)
        {
            UserLogic userLogin = new UserLogic();
            string userName = Request.QueryString["userName"];
            District district = 0;
            Enum.TryParse(DistrictDropDownList.SelectedValue, out district);
            userLogin.ChangeUserDistrict(userName, district);

            Response.Redirect("ChangeDistrict.aspx?userName="+ Request.QueryString["userName"]);
        }
    }
}