﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportByDistrict.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.AccountantReportByDistrict" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
        Costs by District.<br />
        <br />
        <asp:GridView ID="AverageCostsGridView" runat="server" DataSourceID="SqlDataSource" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="DistrictName" HeaderText="DistrictName" SortExpression="DistrictName" />
                <asp:BoundField DataField="TotalLabourHours" HeaderText="TotalLabourHours" ReadOnly="True" SortExpression="TotalLabourHours" />
                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" ReadOnly="True" SortExpression="TotalCost" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
            SelectCommand="select Client.DistrictName, sum(Intervention.LabourRequired) as TotalLabourHours,sum(Intervention.Cost) as TotalCost from Intervention
left join Client on Intervention.ClientId=Client.ClientId
where State='Completed'
group by Client.DistrictName;">

        </asp:SqlDataSource>
        <br />
        Grand-total for the entire organization:<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceGrandTotal">
            <Columns>
                <asp:BoundField DataField="TotalLabourHours" HeaderText="TotalLabourHours" ReadOnly="True" SortExpression="TotalLabourHours" />
                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" ReadOnly="True" SortExpression="TotalCost" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSourceGrandTotal" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select  sum(Intervention.LabourRequired) as TotalLabourHours,sum(Intervention.Cost) as TotalCost from Intervention
where State='Completed';"></asp:SqlDataSource>
        <br />

    </div>

</asp:Content>