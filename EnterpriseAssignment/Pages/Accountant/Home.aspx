﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="EnterpriseAssignment.AccountantHome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>

    
        Accountant Home Dashboard<br />
        <br />
        <br />
        List of all Site Engineers and Managers:<br />
        <br />
        <asp:GridView ID="UsersGridView" runat="server" AutoGenerateColumns="False" DataSourceID="UserListDataSource" OnSelectedIndexChanged="UsersGridView_SelectedIndexChanged">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="LoginUserName" HeaderText="LoginUserName" SortExpression="LoginUserName" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                <asp:BoundField DataField="ResponsibleDistrict" HeaderText="ResponsibleDistrict" SortExpression="ResponsibleDistrict" />
                <asp:BoundField DataField="ApprovableLabour" HeaderText="ApprovableLabour" SortExpression="ApprovableLabour" />
                <asp:BoundField DataField="ApprovableCost" HeaderText="ApprovableCost" SortExpression="ApprovableCost" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="UserListDataSource" runat="server" SelectMethod="ListAllEngineersManagers" TypeName="EnterpriseAssignment.BusinessLogic.UserLogic"></asp:ObjectDataSource>
        <br />
        <br />
        Reporting:<br />
        <br />
        <asp:RadioButton ID="RadioButtonTotalCosts" runat="server" Text="Total Costs by Engineer" GroupName="ReportRadioButton" />
        <br />
        <asp:RadioButton ID="RadioButtonAverageCosts" runat="server" Text="Average Costs by Engineer" GroupName="ReportRadioButton"/>
        <br />
        <asp:RadioButton ID="RadioButtonDistrictCosts" runat="server" Text="Costs by District" GroupName="ReportRadioButton"/>
        <br />
        <asp:RadioButton ID="RadioButtonMonthCostDistrict" runat="server" Text="Monthly Costs for District" GroupName="ReportRadioButton"/>
        <br />
        <br />
        <asp:Button ID="ButtonGenerateReport" runat="server" OnClick="ButtonGenerateReport_Click" Text="Generate Report" />
        <br />
        <br />
        <br />
    
    </div>
</asp:Content>