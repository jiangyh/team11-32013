﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportTotalCosts.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.AccountantReportTotalCosts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
        Total Costs by Engineer.<br />
        <br />
        <asp:GridView ID="AverageCostsGridView" runat="server" DataSourceID="SqlDataSource" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
                <asp:BoundField DataField="TotalLabourHours" HeaderText="TotalLabourHours" ReadOnly="True" SortExpression="TotalLabourHours" />
                <asp:BoundField DataField="TotalCost" HeaderText="TotalCost" ReadOnly="True" SortExpression="TotalCost" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
            SelectCommand="select ProposingName as Username,sum(Intervention.LabourRequired) as TotalLabourHours,sum(Intervention.Cost) as TotalCost from Intervention
left join AspNetUsers on Intervention.ProposingName=AspNetUsers.UserName
left join AspNetUserRoles on AspNetUsers.Id=AspNetUserRoles.UserId
where AspNetUserRoles.RoleId=1 and State='Completed'
group by ProposingName
order by ProposingName;">

        </asp:SqlDataSource>
        <br />

    </div>

</asp:Content>
