﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportAvCosts.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.AccountantReportAvCosts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
        Average Costs by Engineer<br />
        <br />
        <asp:GridView ID="AverageCostsGridView" runat="server" DataSourceID="SqlDataSource" AutoGenerateColumns="False">
            <Columns>
                <asp:BoundField DataField="Username" HeaderText="Username" SortExpression="Username" />
                <asp:BoundField DataField="AvgLabourHours" HeaderText="AvgLabourHours" ReadOnly="True" SortExpression="AvgLabourHours" />
                <asp:BoundField DataField="AvgCost" HeaderText="AvgCost" ReadOnly="True" SortExpression="AvgCost" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
            SelectCommand="select ProposingName as Username,avg(Intervention.LabourRequired) as AvgLabourHours,avg(Intervention.Cost) as AvgCost from Intervention
left join AspNetUsers on Intervention.ProposingName=AspNetUsers.UserName
left join AspNetUserRoles on AspNetUsers.Id=AspNetUserRoles.UserId
where AspNetUserRoles.RoleId=1 and State='Completed'
group by ProposingName
order by ProposingName;">

        </asp:SqlDataSource>
        <br />

    </div>

</asp:Content>
