﻿using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnterpriseAssignment
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            IdentityResult result = manager.ChangePassword(User.Identity.GetUserId(), OldPassword.Text, NewPassword.Text);

            if (result.Succeeded)
            {
                StatusMessage.Text = "Password change success";
            }
            else
            {
                StatusMessage.Text = result.Errors.FirstOrDefault();
            }
            
        }
    }
}