﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class ManagerSeeList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int interventionId = int.Parse(GridViewIntervention.SelectedRow.Cells[1].Text);
            Response.Redirect("InterventionDetail.aspx?interventionId=" + interventionId);
        }
    }
}