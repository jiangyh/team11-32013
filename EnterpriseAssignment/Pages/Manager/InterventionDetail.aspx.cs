﻿using EnterpriseAssignment.Models;
using EnterpriseAssignment.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class Manager_InterventionDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["interventionId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("InterventionList.aspx");
        }

        protected void Approve_Click(object sender, EventArgs e)
        {
            InterventionLogic interventionLogic = new InterventionLogic();
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
            interventionLogic.ApproveIntervention(currentUser, int.Parse(DetailsView1.Rows[0].Cells[1].Text));
            Response.Redirect("InterventionList.aspx");
        }
    }
}