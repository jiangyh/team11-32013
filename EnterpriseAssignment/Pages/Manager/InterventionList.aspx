﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InterventionList.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.ManagerSeeList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
    
        Manager See Interventtion List<br />
        <br />
        <br />
        Proposed Intervention:<br />
        <br />
        <asp:GridView ID="GridViewIntervention" runat="server" AutoGenerateColumns="False" DataSourceID="ListPreposedIntervention" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="InterventionId" HeaderText="InterventionId" SortExpression="InterventionId" />
                <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
                <asp:BoundField DataField="ProposingName" HeaderText="ProposingName" SortExpression="ProposingName" />
                <asp:BoundField DataField="InterventionType" HeaderText="InterventionType" SortExpression="InterventionType" />
                <asp:BoundField DataField="LabourRequired" HeaderText="LabourRequired" SortExpression="LabourRequired" />
                <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
                <asp:BoundField DataField="ApprovalName" HeaderText="ApprovalName" SortExpression="ApprovalName" />
                <asp:BoundField DataField="PerformedDate" HeaderText="PerformedDate" SortExpression="PerformedDate" />
                <asp:BoundField DataField="CurrentState" HeaderText="CurrentState" SortExpression="CurrentState" />
                <asp:BoundField DataField="RecentVisitDate" HeaderText="RecentVisitDate" SortExpression="RecentVisitDate" />
                <asp:BoundField DataField="RemainingLife" HeaderText="RemainingLife" SortExpression="RemainingLife" />
                <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ListPreposedIntervention" runat="server" SelectMethod="ProposedInterventions" TypeName="EnterpriseAssignment.BusinessLogic.InterventionLogic"></asp:ObjectDataSource>
        <br />
        <br />
        <br />
    
    </div>
</asp:Content>