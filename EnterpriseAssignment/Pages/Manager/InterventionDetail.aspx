﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InterventionDetail.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.Manager_InterventionDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
        <div>
            Manager See Interventtion List<br />
            <br />
            <br />
            Proposed Intervention:<br />
            <br />
            <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="InterventionDetail" Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="InterventionId" HeaderText="InterventionId" SortExpression="InterventionId" />
                    <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
                    <asp:BoundField DataField="ProposingName" HeaderText="ProposingName" SortExpression="ProposingName" />
                    <asp:BoundField DataField="InterventionType" HeaderText="InterventionType" SortExpression="InterventionType" />
                    <asp:BoundField DataField="LabourRequired" HeaderText="LabourRequired" SortExpression="LabourRequired" />
                    <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
                    <asp:BoundField DataField="ApprovalName" HeaderText="ApprovalName" SortExpression="ApprovalName" />
                    <asp:BoundField DataField="PerformedDate" HeaderText="PerformedDate" SortExpression="PerformedDate" />
                    <asp:BoundField DataField="CurrentState" HeaderText="CurrentState" SortExpression="CurrentState" />
                    <asp:BoundField DataField="RecentVisitDate" HeaderText="RecentVisitDate" SortExpression="RecentVisitDate" />
                    <asp:BoundField DataField="RemainingLife" HeaderText="RemainingLife" SortExpression="RemainingLife" />
                </Fields>
            </asp:DetailsView>
            <asp:ObjectDataSource ID="InterventionDetail" runat="server" SelectMethod="FindIntervention" TypeName="EnterpriseAssignment.BusinessLogic.InterventionLogic">
                <SelectParameters>
                    <asp:QueryStringParameter Name="interventionId" QueryStringField="interventionId" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:Button ID="Cancel" runat="server" Text="Cancel" OnClick="Cancel_Click" />
            <asp:Button ID="Approve" runat="server" Text="Approve" OnClick="Approve_Click" />
            <br />
            <br />
            <br />
        </div>
</asp:Content>
