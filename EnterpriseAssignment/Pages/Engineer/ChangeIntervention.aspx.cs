﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.BusinessLogic;
using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.Pages.Engineer
{
    public partial class ChangeIntervention : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["interventionId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
            InterventionLogic interventionLogic = new InterventionLogic();
            string interventionId = Request.QueryString["interventionId"];
            interventionLogic.ChangeQualityInformation(currentUser, int.Parse(interventionId), int.Parse(remainingLife.Text), Note.Text);
            Response.Redirect("InterventionDetail.aspx?interventionId=" + interventionId);
        }
    }
}