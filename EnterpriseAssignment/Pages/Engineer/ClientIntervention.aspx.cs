﻿using EnterpriseAssignment.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class ClientIntervention : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["clientId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            InterventionLogic interventionLogic = new InterventionLogic();
            int clientId=0;
            Int32.TryParse(Request.QueryString["clientId"], out clientId);
            InterventionGridView.DataSource = interventionLogic.ClientInterventions(clientId);
            InterventionGridView.DataBind();
        }

        protected void CreateInteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("CreateNewIntervention.aspx?clientId=" + DetailsViewClient.Rows[0].Cells[1].Text);
        }

        protected void BackButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("ListClients.aspx");
        }
    }
}