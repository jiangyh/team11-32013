﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.BusinessLogic;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class AddClient : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            ClientLogic clientLogic = new ClientLogic();

            ClientType clientType = 0;
            Enum.TryParse(ClientTypeDropDownList.SelectedValue, out clientType);

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = manager.FindById(HttpContext.Current.User.Identity.GetUserId());

            clientLogic.CreateNewClient(NameTextBox.Text, clientType, LocationTextBox.Text, user.ResponsibleDistrict, ContactTextBox.Text);

            Response.Redirect("~/Pages/Engineer/ListClients.aspx");
        }
    }
}