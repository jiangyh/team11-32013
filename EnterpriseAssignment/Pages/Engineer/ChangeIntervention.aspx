﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangeIntervention.aspx.cs" Inherits="EnterpriseAssignment.Pages.Engineer.ChangeIntervention" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 239px;
        }
        .auto-style2 {
            width: 239px;
            height: 36px;
        }
        .auto-style3 {
            height: 36px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>

    <br />
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">ENET CARE: </td>
                    <td>Change intervention Quality Management Information:</td>
                </tr>
                <tr>
                    <td class="auto-style2">Remaining life:</td>
                    <td class="auto-style3">
                        <asp:DropDownList ID="remainingLife" runat="server" AutoPostBack="True" ValidateRequestMode="Enabled">
                            <asp:ListItem>100</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>
                            <asp:ListItem>0</asp:ListItem>
                        </asp:DropDownList>
                        *</td>
                </tr>         
                <tr>
                    <td class="auto-style1">Note:</td>
                    <td>
                        <asp:TextBox ID="Note" runat="server" ValidateRequestMode="Enabled" Height="169px" Width="350px" TextMode="MultiLine" MaxLength="4000"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1"></td>
                    <td>
                        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
                    </td>
                </tr>
            </table>
            <br />

</div>


</asp:Content>
