﻿<%@ Page Title="List Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListClients.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.ListClients" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
    
        List Client in current suburb<br />
        <br />
        <a href="AddClient.aspx">Add new client</a>
        <a href="HistoryIntervention.aspx">View intervention history</a>
        
        <br />
        <br />
        <asp:GridView ID="GridViewClient" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewClient_SelectedIndexChanged">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                <asp:BoundField DataField="DistrictName" HeaderText="DistrictName" SortExpression="DistrictName" />
                <asp:BoundField DataField="Contact" HeaderText="Contact" SortExpression="Contact" />
            </Columns>
        </asp:GridView>

    </div>
</asp:Content>

