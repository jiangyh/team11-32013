﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.BusinessLogic;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class CreateNewIntervention : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["clientId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            RangeValidatorCost.Validate();
            RangeValidatorCost.Validate();
            if (Page.IsValid && Cost.Text != null && LabourRequired.Text != null)
            {
                int clientId = int.Parse(Request.QueryString["clientId"]);
                var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
                ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
                InterventionLogic interventionLogic = new InterventionLogic();
                decimal labourRequired = decimal.Parse(LabourRequired.Text);
                decimal cost = decimal.Parse(Cost.Text);
                interventionLogic.CreateNewIntervention(currentUser, clientId, DropDownListType.SelectedValue, labourRequired, cost, Note.Text);
                Response.Redirect("ClientIntervention.aspx?clientId=" + clientId);
            }
        }

        protected void DropDownListType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListType.SelectedIndex == 0)
            {
                LabourRequired.Text = "24";
                Cost.Text = "2000";
            }
            if (DropDownListType.SelectedIndex == 1)
            {
                LabourRequired.Text = "48";
                Cost.Text = "3000";
            }
            if (DropDownListType.SelectedIndex == 2)
            {
                LabourRequired.Text = "36";
                Cost.Text = "1000";
            }
        }

    }
}