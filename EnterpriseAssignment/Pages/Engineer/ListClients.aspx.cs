﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.BusinessLogic;
using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class ListClients : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser user = manager.FindById(HttpContext.Current.User.Identity.GetUserId());

            ClientLogic clientLogic = new ClientLogic();
            List<Client> clients= clientLogic.ListDistrictClients(user.ResponsibleDistrict);
            GridViewClient.DataSource = clients;
            GridViewClient.DataBind();
        }

        protected void GridViewClient_SelectedIndexChanged(object sender, EventArgs e)
        {
            int currentClientId = int.Parse(GridViewClient.SelectedRow.Cells[1].Text);
            Response.Redirect("ClientIntervention.aspx?clientId=" + currentClientId);
        }

    }
}