﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateNewIntervention.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.CreateNewIntervention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
        <div>

            <br/>
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">ENET CARE: </td>
                    <td>Create new intervention</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">InterventionType:</td>
                    <td>
                        <asp:DropDownList ID="DropDownListType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListType_SelectedIndexChanged" ValidateRequestMode="Enabled" CausesValidation="True" ValidationGroup="create">
                            <asp:ListItem>Supply and Install Portable Toilet</asp:ListItem>
                            <asp:ListItem>Hepatitis Avoidance Training</asp:ListItem>
                            <asp:ListItem>Supply and Install Storm-proof Home Kit</asp:ListItem>
                        </asp:DropDownList>
                        *</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">LabourRequired(hours):</td>
                    <td><asp:TextBox ID="LabourRequired" runat="server" ValidateRequestMode="Enabled" TextMode="Number" ViewStateMode="Enabled" CausesValidation="True" ValidationGroup="create"></asp:TextBox>
                        *<asp:RangeValidator ID="RangeValidatorLabourRequired" runat="server" ControlToValidate="LabourRequired" ErrorMessage="Please enter value between 0 - 999" MaximumValue="999" MinimumValue="0" Type="Integer" ValidationGroup="create"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorLabourRequired" runat="server" ControlToValidate="LabourRequired" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">Cost($):</td>
                    <td><asp:TextBox ID="Cost" runat="server" ValidateRequestMode="Enabled" TextMode="Number" ViewStateMode="Enabled" CausesValidation="True" ValidationGroup="create"></asp:TextBox>*<asp:RangeValidator ID="RangeValidatorCost" runat="server" ControlToValidate="Cost" ErrorMessage="Please enter value between 0 - 9999" MaximumValue="9999" MinimumValue="0" Type="Integer" ValidationGroup="create"></asp:RangeValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorCost" runat="server" ControlToValidate="Cost" ErrorMessage="Required"></asp:RequiredFieldValidator>
                    </td>
                </tr>               
                <tr>
                    <td>Note:</td>
                    <td>
                        <asp:TextBox ID="Note" runat="server" ValidateRequestMode="Enabled" Height="169px" Width="350px" TextMode="MultiLine" MaxLength="4000"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" ValidateRequestMode="Enabled" ValidationGroup="create" />
                    </td>
                </tr>
            </table>
            &nbsp;
        <br />
        </div>
</asp:Content>
