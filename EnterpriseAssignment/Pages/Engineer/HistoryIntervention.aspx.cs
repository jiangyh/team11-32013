﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.BusinessLogic;

namespace EnterpriseAssignment.WebFormPages
{
    public partial class HistoryIntervention : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
            InterventionLogic interventionLogic = new InterventionLogic();
            GridViewInterventionHistory.DataSource = interventionLogic.PreviousInterventions(currentUser.UserName);
            GridViewInterventionHistory.DataBind();
        }

        protected void GridViewInterventionHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            Response.Redirect("InterventionDetail.aspx?interventionId=" + GridViewInterventionHistory.SelectedRow.Cells[1].Text);
        }
    }
}