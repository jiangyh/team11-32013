﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InterventionDetail.aspx.cs" Inherits="EnterpriseAssignment.Pages.Engineer.InterventionDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
    
        Intervention Detail<br />
        <br />
        <asp:DetailsView ID="DetailsViewIntervention" runat="server" AutoGenerateRows="False" DataSourceID="Findintervention" Height="50px" Width="125px">
            <Fields>
                <asp:BoundField DataField="InterventionId" HeaderText="InterventionId" SortExpression="InterventionId" />
                <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
                <asp:BoundField DataField="ProposingName" HeaderText="ProposingName" SortExpression="ProposingName" />
                <asp:BoundField DataField="InterventionType" HeaderText="InterventionType" SortExpression="InterventionType" />
                <asp:BoundField DataField="LabourRequired" HeaderText="LabourRequired" SortExpression="LabourRequired" />
                <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
                <asp:BoundField DataField="ApprovalName" HeaderText="ApprovalName" SortExpression="ApprovalName" />
                <asp:BoundField DataField="PerformedDate" HeaderText="PerformedDate" SortExpression="PerformedDate" />
                <asp:BoundField DataField="CurrentState" HeaderText="CurrentState" SortExpression="CurrentState" />
                <asp:BoundField DataField="RecentVisitDate" HeaderText="RecentVisitDate" SortExpression="RecentVisitDate" />
                <asp:BoundField DataField="RemainingLife" HeaderText="RemainingLife" SortExpression="RemainingLife" />
                <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
            </Fields>
        </asp:DetailsView>
        <asp:ObjectDataSource ID="Findintervention" runat="server" SelectMethod="FindIntervention" TypeName="EnterpriseAssignment.BusinessLogic.InterventionLogic">
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="" Name="interventionId" QueryStringField="interventionId" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Change Quality Management Information" Width="375px" />
        <br />
        <asp:Button ID="CancelButton" runat="server" Text="Cancel the intervetion" OnClick="CancelButton_Click" Width="208px" />
        &nbsp;<asp:Button ID="Complete" runat="server" OnClick="Complete_Click" Text="Complete the intervention" Width="240px" />
        <br />
        <asp:Button ID="Back" runat="server" OnClick="Back_Click" Text="Back to Intervention History" Width="266px" />
    
    </div>
</asp:Content>
