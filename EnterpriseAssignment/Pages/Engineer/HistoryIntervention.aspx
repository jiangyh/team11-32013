﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HistoryIntervention.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.HistoryIntervention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
    
        History of Intervention</div>
    <asp:GridView ID="GridViewInterventionHistory" runat="server" AutoGenerateColumns="False" OnSelectedIndexChanged="GridViewInterventionHistory_SelectedIndexChanged">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="InterventionId" HeaderText="InterventionId" SortExpression="InterventionId" />
            <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
            <asp:BoundField DataField="ProposingName" HeaderText="ProposingName" SortExpression="ProposingName" />
            <asp:BoundField DataField="InterventionType" HeaderText="InterventionType" SortExpression="InterventionType" />
            <asp:BoundField DataField="LabourRequired" HeaderText="LabourRequired" SortExpression="LabourRequired" />
            <asp:BoundField DataField="Cost" HeaderText="Cost" SortExpression="Cost" />
            <asp:BoundField DataField="ApprovalName" HeaderText="ApprovalName" SortExpression="ApprovalName" />
            <asp:BoundField DataField="PerformedDate" HeaderText="PerformedDate" SortExpression="PerformedDate" />
            <asp:BoundField DataField="CurrentState" HeaderText="CurrentState" SortExpression="CurrentState" />
            <asp:BoundField DataField="RecentVisitDate" HeaderText="RecentVisitDate" SortExpression="RecentVisitDate" />
            <asp:BoundField DataField="RemainingLife" HeaderText="RemainingLife" SortExpression="RemainingLife" />
            <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note" />
        </Columns>
    </asp:GridView>
    <br />
        
    <table class="auto-style6">
                
    </table>
        
</asp:Content>
