﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EnterpriseAssignment.BusinessLogic;
using EnterpriseAssignment.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.Pages.Engineer
{
    public partial class InterventionDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["interventionId"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            if (DetailsViewIntervention.Rows[8].Cells[1].Text.Equals("Approved"))
            {
                Complete.Visible = true;
            }
            else
            {
                Complete.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChangeIntervention.aspx?interventionId=" + DetailsViewIntervention.Rows[0].Cells[1].Text);
        }

        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("HistoryIntervention.aspx");
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            InterventionLogic interventionLogic = new InterventionLogic();
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
            interventionLogic.CancelIntervention(currentUser, int.Parse(DetailsViewIntervention.Rows[0].Cells[1].Text));
            Response.Redirect("HistoryIntervention.aspx");
        }

        protected void Complete_Click(object sender, EventArgs e)
        {
            InterventionLogic interventionLogic = new InterventionLogic();
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            ApplicationUser currentUser = manager.FindById(User.Identity.GetUserId());
            interventionLogic.CompleteIntervention(currentUser, int.Parse(DetailsViewIntervention.Rows[0].Cells[1].Text));
            Response.Redirect("HistoryIntervention.aspx");
        }
    }
}