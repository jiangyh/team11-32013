﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientIntervention.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.ClientIntervention" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>
        Client Detail<br />
        <br />
        <table style="width:100%;">
            <tr>
                <td class="auto-style1">
                    <asp:DetailsView ID="DetailsViewClient" runat="server" AutoGenerateRows="False" DataSourceID="ClientDetail">
                        <Fields>
                            <asp:BoundField DataField="ClientId" HeaderText="ClientId" SortExpression="ClientId" />
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                            <asp:BoundField DataField="Type" HeaderText="Type" SortExpression="Type" />
                            <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" />
                            <asp:BoundField DataField="DistrictName" HeaderText="DistrictName" SortExpression="DistrictName" />
                            <asp:BoundField DataField="Contact" HeaderText="Contact" SortExpression="Contact" />
                        </Fields>
                    </asp:DetailsView>
                    <asp:ObjectDataSource ID="ClientDetail" runat="server" SelectMethod="FindClient" TypeName="EnterpriseAssignment.BusinessLogic.ClientLogic">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="clientId" QueryStringField="clientId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="InterventionGridView" runat="server">
                    </asp:GridView>
                </td>
                <td class="auto-style1"></td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="CreateInteButton" runat="server" OnClick="CreateInteButton_Click" Text="Create New Intervention" Width="253px" />
                    <br />
                    <asp:Button ID="BackButton" runat="server" OnClick="BackButton_Click" Text="Back To Client List" Width="180px" />
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
</asp:Content>
