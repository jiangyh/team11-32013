﻿<%@ Page Title="Add Client" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddClient.aspx.cs" Inherits="EnterpriseAssignment.WebFormPages.AddClient" %>

<asp:Content ID="ContentHead" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 28px;
        }
    </style>
</asp:Content>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div>

        <br />
        <table>
            <tr>
                <td>Add New Client</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>
                    <asp:TextBox ID="NameTextBox" runat="server" MaxLength="10"></asp:TextBox>
                    &nbsp;*<asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ControlToValidate="NameTextBox" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Location:</td>
                <td>
                    <asp:TextBox ID="LocationTextBox" runat="server" MaxLength="4000"></asp:TextBox>
                    &nbsp;*<asp:RequiredFieldValidator ID="RequiredFieldValidatorLocation" runat="server" ControlToValidate="LocationTextBox" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>ClientType:</td>
                <td>
                    <asp:DropDownList ID="ClientTypeDropDownList" runat="server">
                        <asp:ListItem>Person</asp:ListItem>
                        <asp:ListItem>Family</asp:ListItem>
                        <asp:ListItem>Community</asp:ListItem>
                    </asp:DropDownList>
                    *<asp:RequiredFieldValidator ID="RequiredFieldValidatorType" runat="server" ControlToValidate="ClientTypeDropDownList" ErrorMessage="Required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>Contact:</td>
                <td>
                    <asp:TextBox ID="ContactTextBox" runat="server" MaxLength="50" TextMode="Number"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
                </td>
            </tr>
        </table>
        &nbsp;
    <br />
    </div>
</asp:Content>
