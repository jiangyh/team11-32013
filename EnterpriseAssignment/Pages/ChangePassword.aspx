﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="EnterpriseAssignment.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
        Change Password<br />
        <p>
            <asp:Literal runat="server" ID="StatusMessage" />
        </p>
        User Old Password:<br />
        <asp:TextBox ID="OldPassword" runat="server" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorOld" runat="server" ControlToValidate="OldPassword" ErrorMessage="Required"></asp:RequiredFieldValidator>
        <br />
        <br />
        New Password:<br />
        <asp:TextBox ID="NewPassword" runat="server" MaxLength="100"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidatorNew" runat="server" ControlToValidate="NewPassword" ErrorMessage="Required"></asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Button ID="Submit" runat="server" Text="Submit" OnClick="Submit_Click" />
        <br />
    
</asp:Content>
