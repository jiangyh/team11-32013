﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using System;
using System.IO;

[assembly: OwinStartup(typeof(EnterpriseAssignment.App_Start.Startup))]

namespace EnterpriseAssignment.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // configure "|DataDirectory|" to refer to the correct path
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Login")
            });
        }
    }
}
