USE [ENET_CARE]


IF OBJECT_ID('[dbo].[Intervention]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Intervention]; 
IF OBJECT_ID('[dbo].[InterventionType]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[InterventionType]; 
IF OBJECT_ID('[dbo].[Client]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Client];  
IF OBJECT_ID('[dbo].[Intervention]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AspNetUserRoles]; 
IF OBJECT_ID('[dbo].[InterventionType]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AspNetUsers]; 
IF OBJECT_ID('[dbo].[Client]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AspNetRoles];  
--DROP TABLE IF EXISTS [dbo].[Intervention];
--DROP TABLE IF EXISTS [dbo].[Client];
--DROP TABLE IF EXISTS [dbo].[InterventionType];
--DROP TABLE IF EXISTS [dbo].[State];
DELETE FROM [dbo].[AspNetUserRoles] WHERE 1=1;
DELETE FROM [dbo].[AspNetUsers] WHERE 1=1;
DELETE FROM [dbo].[AspNetRoles] WHERE 1=1;



--insert default user type
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'3', N'Accountant')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2', N'Manager')
INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'1', N'SiteEngineer')

--insert default user account
--	UserName 	Password
--	site1		123123
--	site2		123123
--	manager1	123123
--	accountant1	123123
--INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'3294e26b-8409-41d6-bc9d-be9511939f30', N'Site2', 1, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), NULL, 0, N'AKYNcJW8dlR2EB6WoxoPUoLq8d8RS3EwThbAPnXQdD40kjwaCBxRMj0flTx54giF8g==', N'bafd25fd-8cf7-4324-9178-f60b4cbdc29c', NULL, 0, 0, NULL, 0, 0, N'site2')
--INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b0c35674-ae79-4420-b32a-27734711b81d', N'Man1', 5, CAST(100.00 AS Decimal(18, 2)), CAST(20000.00 AS Decimal(18, 2)), NULL, 0, N'ABlGNRK/79aIV8+FG5AQM2wUZVJ1ZHcKBfYjYPr+yKeRyzt6JidN/t2NGvtaiTSm0g==', N'cb910cce-a9da-4286-9443-053652d80b93', NULL, 0, 0, NULL, 0, 0, N'manager1')
--INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e211de8b-dde0-4b88-8435-351b5450a8b9', N'Acc1', 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), NULL, 0, N'AFPGcUlzONsxvCDTbMssAqEKHM74xxmmAElMjBZN3VjRl+izZLSl1SQxdusuggi82w==', N'ca7c3165-56b9-4c4c-a838-894693510292', NULL, 0, 0, NULL, 0, 0, N'accountant1')
--INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fcb9b587-2718-48ae-880c-eb786aae8389', N'Site1', 5, CAST(30.00 AS Decimal(18, 2)), CAST(2000.00 AS Decimal(18, 2)), NULL, 0, N'AFul6TG/d+gImzKXeJ/5notY+s+L0Xs1AM4UAVfTqLH6DljWxbjFSvEKSdprVnDMnA==', N'f01af221-4e1a-4a07-bd8f-4cb4b9da48ce', NULL, 0, 0, NULL, 0, 0, N'site1')

INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'1030e236-4264-4e4c-8ade-77175e97fb4a', N'Kate', 0, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'kate@enet.com', 0, N'AM5zFXaoBfWwAZwIoFNBfj/TuYS/GKhGUcburSEyZaWEeHs8jqyZ8tv/E6tXZMl80g==', N'96180774-4379-40c5-a468-dfa6cd9d7e6d', N'0123456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Kate')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'3294e26b-8409-41d6-bc9d-be9511939f30', N'Site2', 1, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'site2@enet.com', 0, N'AKYNcJW8dlR2EB6WoxoPUoLq8d8RS3EwThbAPnXQdD40kjwaCBxRMj0flTx54giF8g==', N'bafd25fd-8cf7-4324-9178-f60b4cbdc29c', N'2323456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'site2')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'40066054-98d8-4682-ae1a-c3e9d6b1d2ab', N'Rachel', 2, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'rachel@enet.com', 0, N'ACmJnhsuNGcGIf6g+3hpu0NRVYkVwQT0FW8AaDqbJJqdbWeo5WeucbrUDjYE73a+VA==', N'686ca405-233e-454e-83df-8cf496fdb315', N'0223456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Rachel')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'59762cb0-24e1-4dd1-ad59-06827523e0f6', N'Jane', 3, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'jane@enet.com', 0, N'ALmPh21hDSuGKtKYasmGVzShwFmARW/fTmHPpZNqSLNVrr1ZStq9DWNr48Lml0TlXQ==', N'856901be-2dbc-415d-ae7e-bb70a3645604', N'2123456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Jane')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'653b3c3e-f5f8-4daa-8d1a-a37f3da258f5', N'John', 3, CAST(40.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), N'john@enet.com', 0, N'AKVQpqptJfM+7ADY70tm8UKPs3o3rWAu9tHBIMg9Q+658U5CgPlUJoItbTmmJwBncg==', N'ea836846-ab3d-486f-a99a-20cd719a6f4b', N'0323456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'John')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'7277093f-1d4c-4002-81e5-56970f087931', N'James', 2, CAST(50.00 AS Decimal(18, 2)), CAST(4000.00 AS Decimal(18, 2)), N'james@enet.com', 0, N'ANTo4nCpRtwsaZKUt+mGC26GCqGU3VzLkLkDjmI6pFTOCd3mljej7MDfcDqXsdfXsA==', N'0a127a35-ecae-4590-81a3-07d08246f192', N'1923456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'James')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'a74e22de-30db-4c94-b5a3-f31725dc4aef', N'Charlotte', 4, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'charlotte@enet.com', 0, N'ACGnU/NwHNIk54qVpAE0Zgu1MxhwXc2aK4OcBAP9Hh5EUDe7BNcCYp+9U78WwJKXEg==', N'055652e6-8720-4e8c-ad54-795995fa6c44', N'4123456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Charlotte')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'b0c35674-ae79-4420-b32a-27734711b81d', N'Man1', 5, CAST(100.00 AS Decimal(18, 2)), CAST(20000.00 AS Decimal(18, 2)), N'man1@enet.com', 0, N'ABlGNRK/79aIV8+FG5AQM2wUZVJ1ZHcKBfYjYPr+yKeRyzt6JidN/t2NGvtaiTSm0g==', N'cb910cce-a9da-4286-9443-053652d80b93', N'5123456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'manager1')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'be61bfb0-4cb7-4e83-bb60-63808aefd98d', N'Freddy', 0, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'freddy@enet.com', 0, N'AP3kM0b0Naz4mAqOTZKzVC1pOPHdgcPyufKJ4//FBsct0mp0CzaHcnZq5dnY8rhcKA==', N'0ff6781f-6199-4b30-927f-39003e77da75', N'0623456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Freddy')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c465a2a5-7ed0-47f2-88ac-4b0f896082e0', N'Charis', 1, CAST(50.00 AS Decimal(18, 2)), CAST(5000.00 AS Decimal(18, 2)), N'charis@enet.com', 0, N'ACdMqbnbd5BzJu+E4aEjZ2uf2PiCG3H/hpNPpJAhxvqYBdX3Y8BUz7i0awvstwv85Q==', N'3482bc18-c338-44b9-82a0-ee2c1012bd79', N'7123456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'Charis')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'e211de8b-dde0-4b88-8435-351b5450a8b9', N'Acc1', 0, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), N'acc1@enet.com', 0, N'AFPGcUlzONsxvCDTbMssAqEKHM74xxmmAElMjBZN3VjRl+izZLSl1SQxdusuggi82w==', N'ca7c3165-56b9-4c4c-a838-894693510292', N'9923456789', 1, 0, N'2017-01-03 11:50:22', 0, 0, N'accountant1')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Name], [ResponsibleDistrict], [ApprovableLabour], [ApprovableCost], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'fcb9b587-2718-48ae-880c-eb786aae8389', N'Site1', 5, CAST(30.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), N'site1@enet.com', 0, N'AFul6TG/d+gImzKXeJ/5notY+s+L0Xs1AM4UAVfTqLH6DljWxbjFSvEKSdprVnDMnA==', N'f01af221-4e1a-4a07-bd8f-4cb4b9da48ce', N'9823456789', 0, 0, NULL, 0, 0, N'site1')



--assign user a role
--INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3294e26b-8409-41d6-bc9d-be9511939f30', N'1')
--INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fcb9b587-2718-48ae-880c-eb786aae8389', N'1')
--INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b0c35674-ae79-4420-b32a-27734711b81d', N'2')
--INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e211de8b-dde0-4b88-8435-351b5450a8b9', N'3')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3294e26b-8409-41d6-bc9d-be9511939f30', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'fcb9b587-2718-48ae-880c-eb786aae8389', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b0c35674-ae79-4420-b32a-27734711b81d', N'2')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'e211de8b-dde0-4b88-8435-351b5450a8b9', N'3')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1030e236-4264-4e4c-8ade-77175e97fb4a', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'40066054-98d8-4682-ae1a-c3e9d6b1d2ab', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'59762cb0-24e1-4dd1-ad59-06827523e0f6', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'653b3c3e-f5f8-4daa-8d1a-a37f3da258f5', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'7277093f-1d4c-4002-81e5-56970f087931', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a74e22de-30db-4c94-b5a3-f31725dc4aef', N'1')
INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'be61bfb0-4cb7-4e83-bb60-63808aefd98d', N'1')



--create Client table
CREATE TABLE [dbo].[Client] (
    [ClientId]     INT        IDENTITY (1, 1) NOT NULL,
    [Name]         NCHAR (10) NULL,
    [Type]         NVARCHAR(50) NULL,
    [Location]     NVARCHAR(MAX) NULL,
    [DistrictName] NVARCHAR(50) NULL,
    [Contact]      NVARCHAR(50) NULL,
    PRIMARY KEY CLUSTERED ([ClientId] ASC)
);

--insert example data for Client
--INSERT INTO [dbo].[Client] VALUES ( N'Allen', N'Person', N'Hurstville', N'Sydney', N'0123456789')
--INSERT INTO [dbo].[Client] VALUES ( N'Jack', N'Person', N'Eastwood', N'Sydney', N'9876543210')
--INSERT INTO [dbo].[Client] VALUES ( N'Mike', N'Person', N'Chatstwood', N'Sydney', N'6547893210')

SET IDENTITY_INSERT [dbo].[Client] ON
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (1, N'Allen     ', N'Person', N'Hurstville', N'Sydney', N'0123456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (2, N'Jack      ', N'Person', N'Eastwood', N'Sydney', N'9876543210')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (3, N'Mike      ', N'Person', N'Chatstwood', N'Sydney', N'6547893210')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (4, N'James     ', N'Family', N'Condoblin', N'RuralNewSouthWales', N'0144456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (5, N'Heidi     ', N'Family', N'Bourke', N'RuralNewSouthWales', N'0123456689')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (6, N'Frank     ', N'Person', N'WesternHighlands', N'RuralPapuaNewGuinea', N'9923456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (7, N'George    ', N'Family', N'MtHagen', N'RuralPapuaNewGuinea', N'0123654789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (8, N'John      ', N'Person', N'PortMoresby', N'UrbanPapuaNewGuinea', N'9876456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (9, N'Charlotte ', N'Family', N'Bangkanai', N'RuralIndonesia', N'2343456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (10, N'MUBC      ', N'Community', N'Tungka', N'RuralIndonesia', N'0123333789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (11, N'Virginia  ', N'Person', N'Sikan', N'UrbanIndonesia', N'0923456789')
INSERT INTO [dbo].[Client] ([ClientId], [Name], [Type], [Location], [DistrictName], [Contact]) VALUES (12, N'Toni      ', N'Person', N'Dampar', N'UrbanIndonesia', N'0823456789')
SET IDENTITY_INSERT [dbo].[Client] OFF


--create interventionType table
CREATE TABLE [dbo].[InterventionType] (
    [TypeName]      NCHAR (50) NOT NULL,
    [DefaultLabour] DECIMAL NOT NULL,
    [DefaultCost]   DECIMAL NOT NULL,
    PRIMARY KEY CLUSTERED ([TypeName] ASC)
);


--set defult intervetionType
INSERT INTO [dbo].[InterventionType] VALUES (N'Supply and Install Portable Toilet', 24, 2000)
INSERT INTO [dbo].[InterventionType] VALUES (N'Hepatitis Avoidance Training', 48, 3000)
INSERT INTO [dbo].[InterventionType] VALUES (N'Supply and Install Storm-proof Home Kit', 36, 1000)

--create intervention table
CREATE TABLE [dbo].[Intervention]
(
	[InterventionId] INT        IDENTITY (1, 1) NOT NULL, 
    [ClientId] INT NOT NULL, 
    [ProposingName] NVARCHAR(256) NOT NULL, 
    [InterventionType] NCHAR(50) NOT NULL, 
    [LabourRequired] DECIMAL NOT NULL, 
    [Cost] DECIMAL NOT NULL, 
    [ApprovalName] NCHAR(50) NULL, 
    [PerformedDate] DATETIME NULL, 
    [State] NCHAR(10) NOT NULL, 
    [RecentVisitDate] DATETIME NULL, 
    [RemainingLife] INT NOT NULL, 
	[Note] NVARCHAR(MAX) NULL,
    CONSTRAINT [FK_Intervention_InterventionType] FOREIGN KEY ([InterventionType]) REFERENCES [InterventionType]([TypeName]), 
    CONSTRAINT [FK_Intervention_ClientId] FOREIGN KEY ([ClientId]) REFERENCES [Client]([ClientId]), 
    CONSTRAINT [FK_Intervention_AspNetUsers] FOREIGN KEY ([ProposingName]) REFERENCES [AspNetUsers]([UserName]), 
	PRIMARY KEY CLUSTERED ([InterventionId] ASC)
);

--insert example data for intervention
--INSERT INTO [dbo].[Intervention] VALUES (1, N'site1', N'Supply and Install Portable Toilet', 24, 2000, NULL, GETDATE(), N'proposed', GETDATE() , 100, NULL)
--INSERT INTO [dbo].[Intervention] VALUES (1, N'site2', N'Hepatitis Avoidance Training', 48, 3000, NULL, GETDATE() , N'approved', GETDATE() , 100, NULL)

SET IDENTITY_INSERT [dbo].[Intervention] ON
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (1, 1, N'site1', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 13:14:35', N'Approved  ', N'2017-05-03 13:14:35', 100, N'On time and to budget')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (2, 1, N'Charis', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 13:14:35', N'Approved  ', N'2017-05-03 13:14:35', 100, N'On time!')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (5, 2, N'Charis', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 13:14:35', N'Proposed  ', N'2017-05-03 13:14:35', 100, N'Please approve asap')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (11, 3, N'site2', N'Supply and Install Storm-proof Home Kit           ', CAST(36 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 13:14:35', N'Proposed  ', N'2017-05-03 13:14:35', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (12, 12, N'Freddy', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 11:50:22', N'Approved  ', N'2017-05-03 11:50:22', 100, N'Complete!')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (13, 11, N'Freddy', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 11:50:22', N'Approved  ', N'2017-01-03 11:50:22', 100, N'On time, small problem with delivery of materials (arrived late)')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (14, 4, N'Kate', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 11:50:22', N'Approved  ', N'2016-12-03 09:50:22', 100, N'Again, a delay in delivery. Can we use a different courier!?!?')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (15, 5, N'Kate', N'Hepatitis Avoidance Training                      ', CAST(48 AS Decimal(18, 0)), CAST(3000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-05-03 11:50:22', N'Approved  ', N'2017-05-03 11:50:22', 100, N'This was the on time.')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (16, 6, N'Charlotte', N'Supply and Install Storm-proof Home Kit           ', CAST(36 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), N'                                                  ', N'2017-04-13 11:50:22', N'Proposed  ', N'2017-04-13 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (17, 7, N'Charlotte', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(500 AS Decimal(18, 0)), N'                                                  ', N'2017-02-12 11:50:22', N'Proposed  ', N'2017-02-12 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (18, 8, N'Jane', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-05-03 11:50:22', N'Proposed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (19, 9, N'James', N'Supply and Install Storm-proof Home Kit           ', CAST(36 AS Decimal(18, 0)), CAST(350 AS Decimal(18, 0)), N'                                                  ', N'2017-05-03 11:50:22', N'Proposed  ', N'2017-05-03 11:50:22', 100, N'Some equipment already provided - cheaper')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (20, 10, N'James', N'Supply and Install Storm-proof Home Kit           ', CAST(36 AS Decimal(18, 0)), CAST(1000 AS Decimal(18, 0)), N'                                                  ', N'2017-01-03 11:50:22', N'Proposed  ', N'2017-01-03 11:50:22', 100, N'MUBC community hall was very happy!')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (21, 8, N'John', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-01-03 11:50:22', N'Proposed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (22, 2, N'Charis', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-01-03 11:50:22', N'Proposed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (23, 1, N'site1', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-01-03 11:50:22', N'Proposed  ', N'2017-05-03 13:14:35', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (24, 2, N'site1', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-05-01 13:14:35', N'Proposed  ', N'2017-05-01 13:14:35', 100, N'Please approve asap')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (25, 2, N'site1', N'Supply and Install Portable Toilet                ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'                                                  ', N'2017-05-03 13:14:35', N'Proposed  ', N'2017-05-03 13:14:35', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (26, 8, N'Jane', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-12 10:50:22', N'Completed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (27, 2, N'Charis', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-12 10:50:22', N'Completed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (28, 1, N'site1', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-12 10:50:22', N'Completed  ', N'2017-05-03 11:50:22', 100, N'Great job')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (29, 2, N'site1', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-17 10:50:22', N'Completed  ', N'2017-05-03 11:50:22', 100, N'Good work!')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (30, 2, N'site1', N'Supply and Install Storm-proof Home Kit           ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-17 10:50:22', N'Completed  ', N'2017-05-03 11:50:22', 100, N'')
INSERT INTO [dbo].[Intervention] ([InterventionId], [ClientId], [ProposingName], [InterventionType], [LabourRequired], [Cost], [ApprovalName], [PerformedDate], [State], [RecentVisitDate], [RemainingLife], [Note]) VALUES (31, 8, N'Jane', N'Hepatitis Avoidance Training                      ', CAST(24 AS Decimal(18, 0)), CAST(2000 AS Decimal(18, 0)), N'Man1                                              ', N'2017-01-17 10:50:22', N'Completed ', N'2017-05-03 11:50:22', 100, N'')
SET IDENTITY_INSERT [dbo].[Intervention] OFF