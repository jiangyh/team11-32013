Include a simple �README� file that includes instructions for how to run your solution:

"ENET CARE Inventory Management System" 03/05/2017

Team 11
Members:
Zhen Wang 11937796
Lu li 12170724
Jiang Yihao 12176794
William Thomson 11860766

Running the program:

	-Start Visual Studio
	-Build the solution (toolbar -> Build, Build Solution)
	-Run solution in browser of your choice
	-You will now see a login page

You can use the following details to log in:

	To log in as a Manager:
	Username: manager1
	Password: 123123


	To log in as an Engineer:
	Username: site1
	Password: 123123

	To log in as an Accountant:
	Username: accountant1
	Password: 123123

Troubleshooting:

	If project does not launch correctly, please launch after selecting the login page.
	If you are not able to login with above details, you may need to build the database from the script.

	-Open the file under the folder Database_Script
	-Using the toolbar, 'connect' to your local database
	-Execute the script

	Then launch from login page and use above login details.