﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.Models
{
    public class ApplicationUser : IdentityUser
    {
        //public string LoginUsername { get; set; }
        public string Name { get; set; }
        //public string Password { get; set; }
        //public EngineerType Type { get; set; }
        public District ResponsibleDistrict { get; set; }
        public decimal ApprovableLabour { get; set; }
        public decimal ApprovableCost { get; set; }

        public ApplicationUser() : base()
        {
            
        }

        public ApplicationUser(string loginUsername, string name, string password)
        {
            UserName = loginUsername;
            Name = name;
            PasswordHash = password;
        }

        public ApplicationUser(string loginUsername, string password)//testing for data access
        {
            UserName = loginUsername;
            PasswordHash = password;
        }

        public ApplicationUser(string loginUsername, string name, string password, EngineerType type,
            District responsibleDistrict, decimal approvableLabour, decimal approvableCost)
        {
            UserName = loginUsername;
            Name = name;
            PasswordHash = password;
            //Type = type;
            ResponsibleDistrict = responsibleDistrict;
            ApprovableLabour = approvableLabour;
            ApprovableCost = approvableCost;
        }
        
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }


    public enum EngineerType
    {
        SiteEngineer, Manager, Accountant
    }

    public class ApplicationUserInfo {

        public string LoginUserName { get; set; }
        public string Name { get; set; }
        public EngineerType Type { get; set; }
        public District ResponsibleDistrict { get; set; }
        public decimal ApprovableLabour { get; set; }
        public decimal ApprovableCost { get; set; }
    }

}