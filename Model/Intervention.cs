﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace EnterpriseAssignment.Models
{
    public class Intervention
    {
        public int InterventionId { get; set; }
        public int ClientId { get; set; }           //map to client
        public string ProposingName { get; set; }   //map to user
        public string InterventionType { get; set; }
        public decimal LabourRequired { get; set; }
        public decimal Cost { get; set; }
        public string ApprovalName { get; set; }    //map to user
        public DateTime PerformedDate { get; set; }
        public State CurrentState { get; set; }
        public DateTime RecentVisitDate { get; set; }
        public int RemainingLife { get; set; }
        public string Note { get; set; }

        private static decimal id = 0;

        //intervention data model with interventionId, clientId, proposingName, interventionType, labourRequired, cost,
        //approvalName, performedDate, currentState, recentVisitDate, remainingLife, note parameter
        public Intervention(int interventionId, int clientId, string proposingName, string interventionType, decimal labourRequired, decimal cost,
            string approvalName, DateTime performedDate, State currentState, DateTime recentVisitDate, int remainingLife, string note)
        {
            InterventionId = interventionId;
            ClientId = clientId;
            ProposingName = proposingName;
            InterventionType = interventionType;
            LabourRequired = labourRequired;
            Cost = cost;
            ApprovalName = approvalName;
            PerformedDate = performedDate;
            CurrentState = currentState;
            RecentVisitDate = recentVisitDate;
            RemainingLife = remainingLife;
            Note = note;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static string GetUID()
        {
            return id++.ToString();
        }
    }
    //four state
    public enum State
    {
        Proposed, Approved, Completed, Cancelled
    }
}