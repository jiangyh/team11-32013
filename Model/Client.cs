﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;

namespace EnterpriseAssignment.Models
{
    public class Client
    {
        public int ClientId { get; set; }
        public string Name { get; set; }
        public ClientType Type { get; set; }
        public string Location { get; set; }
        public District DistrictName { get; set; }
        public string Contact { get; set; }

        public Client()
        {

        }

        //set client with name, type, location, districtName, contact
        public Client(string name, ClientType type, string location, District districtName, string contact)
        {
            Name = name;
            Type = type;
            Location = location;
            DistrictName = districtName;
            Contact = contact;
        }

        public Client(int clientId, string name, ClientType type, string location, District districtName, string contact)
        {
            ClientId = clientId;
            Name = name;
            Type = type;
            Location = location;
            DistrictName = districtName;
            Contact = contact;
        }
    }
    //three client type
    public enum ClientType
    {
        Person, Family, Community
    }
    //several district
    public enum District
    {
        NotInAnyDistrict,
        UrbanIndonesia,
        RuralIndonesia,
        UrbanPapuaNewGuinea,
        RuralPapuaNewGuinea,
        Sydney,
        RuralNewSouthWales
    }
}