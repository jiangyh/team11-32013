﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnterpriseAssignment.Models
{
    public class InterventionType
    {
        public string TypeName { get; set; }
        public decimal LabourRequired { get; set; }
        public decimal Cost { get; set; }

        public InterventionType()
        {

        }

        //interventionType with typeName, labourRequired, cost parameter
        public InterventionType(string typeName, decimal labourRequired, decimal cost)
        {
            TypeName = typeName;
            LabourRequired = labourRequired;
            Cost = cost;
        }
    }
}
