﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.DataAccess;

namespace EnterpriseAssignment.BusinessLogic
{

    public class ClientLogic
    {
        // show all clients
        public List<Client> ListAllClients()
        {
            ClientDataAccess dataAccess = new ClientDataAccess();
            return dataAccess.ReadAllClients();

        }
        //show clients in current user district
        public List<Client> ListDistrictClients(District district)
        {
            ClientDataAccess dataAccess = new ClientDataAccess();
            return dataAccess.ReadDistrictClients(district);
        }

        //find a specific client
        public Client FindClient(int clientId)
        {
            ClientDataAccess dataAccess = new ClientDataAccess();
            return dataAccess.ReadAllClients().Find(x => x.ClientId == clientId);
        }

        //add new client into database
        public void CreateNewClient(string name, ClientType type, string location, District districtName, string contact)
        {
            ClientDataAccess clientDataAccess = new ClientDataAccess();
            clientDataAccess.Create(new Client(name, type, location, districtName, contact));
        }
    }
}