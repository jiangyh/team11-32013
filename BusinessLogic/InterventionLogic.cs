﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.DataAccess;
using Microsoft.AspNet.Identity.EntityFramework;

namespace EnterpriseAssignment.BusinessLogic
{

    public class InterventionLogic
    {
        //add new intervention
        public string CreateNewIntervention(ApplicationUser currentUser, int clientId, string interventionTypeName, decimal labourRequired, decimal cost, string note)
        {
            if (currentUser != null)
            {
                InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
                InterventionType interventionType = interventionDataAccess.FindDefualType(interventionTypeName);

                string ProposingName = currentUser.UserName;
                DateTime performedDate = DateTime.Now;
                DateTime recentVisitDate = DateTime.Now;
                int remainingLife = 100;

                string approvalName;
                State currentState;
                if (currentUser.ApprovableCost >= cost && currentUser.ApprovableLabour >= labourRequired &&
                    currentUser.ApprovableCost >= interventionType.Cost && currentUser.ApprovableLabour >= interventionType.LabourRequired)
                {
                    currentState = State.Completed;
                    approvalName = ProposingName;
                }
                else
                {
                    currentState = State.Proposed;
                    approvalName = "";
                }


                interventionDataAccess.Create(clientId, ProposingName, interventionTypeName, labourRequired, cost,
                    approvalName, performedDate, currentState, recentVisitDate, remainingLife, note);
                return "Create Intervention Success";
            }
            else
            {
                return "Need Login";
            }

        }


        //For uni test about create new intervetnion
        public State CreateNewInterventionTest(ApplicationUser currentUser, InterventionType interventionType, decimal labourRequired, decimal cost)
        {
            if (currentUser.ApprovableCost >= cost && currentUser.ApprovableLabour >= labourRequired &&
                    currentUser.ApprovableCost >= interventionType.Cost && currentUser.ApprovableLabour >= interventionType.LabourRequired)
            {
                return State.Completed;
            }
            else
            {
                return State.Proposed;
            }
        }



        //find proposed interventions
        public List<Intervention> ProposedInterventions()
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            return interventionDataAccess.ListProposedInterventions();
        }

        //find interventions related to specific client
        public List<Intervention> ClientInterventions(int clientId)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            return interventionDataAccess.ListClientInterventions(clientId);
        }

        //list history interventions
        public List<Intervention> PreviousInterventions(string userName)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            return interventionDataAccess.ListPreviousInterventions(userName);
        }

        //find specific intervention
        public Intervention FindIntervention(int interventionId)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            return interventionDataAccess.ListInterventions().Find(x => x.InterventionId == interventionId);
        }

        //approve a intervention
        public string ApproveIntervention(ApplicationUser currentUser, int interventionId)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            Intervention intervention = FindIntervention(interventionId);
            if (currentUser.ApprovableCost >= intervention.Cost && currentUser.ApprovableLabour >= intervention.LabourRequired)
            {
                interventionDataAccess.changeState(intervention.InterventionId, State.Approved);
                return "Success";
            }
            else
            {
                return "Need higher Approval";
            }
        }

        //used for testing approve intervention method
        public string ApproveInterventionTest(ApplicationUser currentUser, decimal labourRequired, decimal cost)
        {
            if (currentUser.ApprovableCost >= cost && currentUser.ApprovableLabour >= labourRequired)
            {
                return "Success";
            }
            else
            {
                return "Need higher Approval";
            }
        }

        //cancel a specific intervention
        public string CancelIntervention(ApplicationUser currentUser, int interventionId)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            Intervention intervention = FindIntervention(interventionId);
            if (currentUser.UserName == intervention.ProposingName)
            {
                interventionDataAccess.changeState(intervention.InterventionId, State.Cancelled);
                return "Success";
            }
            else
            {
                return "Sorry, you cannot cancel the intervention.";
            }
        }

        //change a intervention to complete
        public string CompleteIntervention(ApplicationUser currentUser, int interventionId)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            Intervention intervention = FindIntervention(interventionId);
            if (currentUser.UserName == intervention.ProposingName)
            {
                interventionDataAccess.changeState(intervention.InterventionId, State.Completed);
                return "Success";
            }
            else
            {
                return "Sorry, you cannot complete the intervention.";
            }
        }

        //used to change quality information of intervention
        public string ChangeQualityInformation(ApplicationUser currentUser, int interventionId, int remainingLife, string note)
        {
            InterventionDataAccess interventionDataAccess = new InterventionDataAccess();
            Intervention intervention = FindIntervention(interventionId);
            if (currentUser.UserName == intervention.ProposingName)
            {
                interventionDataAccess.changeQualityInformation(intervention.InterventionId, DateTime.Now, remainingLife, note);
                return "Success";
            }
            else
            {
                return "Sorry, you cannot complete the intervention.";
            }
        }
    }
}