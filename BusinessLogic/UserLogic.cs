﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EnterpriseAssignment.Models;
using EnterpriseAssignment.DataAccess;

namespace EnterpriseAssignment.BusinessLogic
{

    public class UserLogic
    {

        public List<ApplicationUserInfo> ListAllEngineersManagers()
        {
            UserDataAccess dataAccess = new UserDataAccess();
            return dataAccess.ReadAllEngineersManagers();
        }

        public ApplicationUserInfo GetUserInfoByUserName(string userName)
        {
            UserDataAccess dataAccess = new UserDataAccess();
            return dataAccess.ReadUserByUserName(userName);
        }

        public void ChangeUserDistrict(string userName, District district)
        {
            UserDataAccess dataAccess = new UserDataAccess();
            dataAccess.changeDistrict(userName,district);
        }

    }
}