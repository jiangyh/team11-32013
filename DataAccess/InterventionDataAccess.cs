﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAssignment.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace EnterpriseAssignment.DataAccess
{
    public class InterventionDataAccess
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //create new intervention
        public void Create(int clientId,
                            string proposingName,
                            string interventionType,
                            decimal labourRequired,
                            decimal cost,
                            string approvalName,
                            DateTime performedDate,
                            State currentState,
                            DateTime recentVisitDate,
                            int remainingLife,
                            string note)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"INSERT INTO Intervention VALUES (
                                @clientId, 
                                @proposingName, 
                                @interventionType, 
                                @labourRequired, 
                                @cost, 
                                @approvalName,
                                @performedDate,
                                @currentState,
                                @recentVisitDate,
                                @remainingLife,
                                @note)";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("clientId", clientId));
                command.Parameters.Add(new SqlParameter("proposingName", proposingName));
                command.Parameters.Add(new SqlParameter("interventionType", interventionType));
                command.Parameters.Add(new SqlParameter("labourRequired", labourRequired));
                command.Parameters.Add(new SqlParameter("cost", cost));
                command.Parameters.Add(new SqlParameter("approvalName", approvalName));
                command.Parameters.Add(new SqlParameter("performedDate", performedDate));
                command.Parameters.Add(new SqlParameter("currentState", currentState.ToString()));
                command.Parameters.Add(new SqlParameter("recentVisitDate", recentVisitDate));
                command.Parameters.Add(new SqlParameter("remainingLife", remainingLife));
                command.Parameters.Add(new SqlParameter("note", note));
                command.ExecuteNonQuery();

                conn.Close();
            }
        }

        //List all the interventions proposed by specific engineer
        public List<Intervention> ListPreviousInterventions(string userName)
        {
            List<Intervention> interventions = ListInterventions();
            List<Intervention> previousInterventions = new List<Intervention>();
            foreach (Intervention tempintervention in interventions)
            {
                if (tempintervention.ProposingName.Equals(userName))
                {
                    previousInterventions.Add(tempintervention);
                }
            }
            return previousInterventions;
        }

        //List all the interventions related to specific client
        public List<Intervention> ListClientInterventions(int clientId)
        {
            List<Intervention> interventions = ListInterventions();
            List<Intervention> clientInterventions = new List<Intervention>();
            foreach (Intervention tempintervention in interventions)
            {
                if (tempintervention.ClientId == clientId)
                {
                    clientInterventions.Add(tempintervention);
                }
            }
            return clientInterventions;
        }

        //List all the proposed interventions
        public List<Intervention> ListProposedInterventions()
        {
            List<Intervention> interventions = ListInterventions();
            List<Intervention> previousInterventions = new List<Intervention>();
            foreach (Intervention tempintervention in interventions)
            {
                if (tempintervention.CurrentState.Equals(State.Proposed))
                {
                    previousInterventions.Add(tempintervention);
                }
            }
            return previousInterventions;
        }

        //List all the intervention in the database
        public List<Intervention> ListInterventions()
        {
            List<Intervention> interventions = new List<Intervention>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"select * from Intervention where State <> N'Cancelled'";
                SqlCommand command = new SqlCommand(query, conn);

                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        int interventionId = result.GetInt32(0);
                        int clientId = result.GetInt32(1);
                        string proposingName = result.GetString(2);
                        string interventionType = result.GetString(3);
                        Decimal labourRequired = result.GetDecimal(4);
                        Decimal cost = result.GetDecimal(5);
                        string approvalName;
                        //approvalName = result.GetString(6);
                        DateTime performedDate;
                        //performedDate = result.GetDateTime(7);
                        State currentState;
                        Enum.TryParse(result.GetString(8), true, out currentState);
                        DateTime recentVisitDate;
                        //recentVisitDate = result.GetDateTime(9);
                        int remainingLife = result.GetInt32(10);
                        string note;
                        try
                        {
                            approvalName = result.GetString(6);
                        }
                        catch (SqlNullValueException)
                        {
                            approvalName = "";
                        }
                        try
                        {
                            performedDate = result.GetDateTime(7);
                        }
                        catch (SqlNullValueException)
                        {
                            performedDate = DateTime.MinValue;
                        }
                        try
                        {
                            recentVisitDate = result.GetDateTime(9);
                        }
                        catch (SqlNullValueException)
                        {
                            recentVisitDate = DateTime.MinValue;
                        }
                        try
                        {
                            note = result.GetString(11);
                        }
                        catch (SqlNullValueException)
                        {
                            note = "";
                        }

                        interventions.Add(
                            new Intervention(
                                interventionId,
                                clientId,
                                proposingName,
                                interventionType,
                                labourRequired,
                                cost,
                                approvalName,
                                performedDate,
                                currentState,
                                recentVisitDate,
                                remainingLife,
                                note)
                            );
                    }
                }
                conn.Close();
            }
            return interventions;
        }

        //change current state of intervention
        public void changeState(int interventionId, State state)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"UPDATE Intervention 
                                SET State = @state   
                                WHERE InterventionId = @interventionId";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("state", state.ToString()));
                command.Parameters.Add(new SqlParameter("interventionId", interventionId));
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        //change quality information of intervention
        public void changeQualityInformation(int interventionId, DateTime recentVisitDate, int remainingLife, string note)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"UPDATE Intervention 
                                SET RecentVisitDate = @recentVisitDate,
                                    RemainingLife = @remainingLife,
                                    Note = @note
                                WHERE InterventionId = @interventionId";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("recentVisitDate", recentVisitDate));
                command.Parameters.Add(new SqlParameter("remainingLife", remainingLife));
                command.Parameters.Add(new SqlParameter("note", note));
                command.Parameters.Add(new SqlParameter("interventionId", interventionId));
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

        public InterventionType FindDefualType(string typeName)
        {
            List<InterventionType> interventionTypes = new List<InterventionType>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"select * from InterventionType where TypeName = @typeName";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("typeName", typeName));
                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        InterventionType tempType = new InterventionType();
                        tempType.TypeName = result.GetString(0);
                        tempType.LabourRequired = result.GetDecimal(1);
                        tempType.Cost = result.GetDecimal(2);
                        interventionTypes.Add(tempType);
                    }
                }
                conn.Close();
            }
            return interventionTypes[0];
        }
    }


}
