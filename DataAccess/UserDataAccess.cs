﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAssignment.Models;
using System.Configuration;
using System.Data.SqlClient;

namespace EnterpriseAssignment.DataAccess
{
    public class UserDataAccess
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //most queries branch multiple tables... Users, UserRoles, UserClaims etc


        private ApplicationUserInfo userInfoBuilder(SqlDataReader result)
        {
            ApplicationUserInfo user = new ApplicationUserInfo();

            user.Name = result.GetString(0);
            user.ResponsibleDistrict = (District)result.GetInt32(1);
            user.ApprovableLabour = result.GetDecimal(2);
            user.ApprovableCost = result.GetDecimal(3);
            user.LoginUserName = result.GetString(4);
            EngineerType type = 0;
            Enum.TryParse(result.GetString(5), out type);
            user.Type = type;

            return user;
        }

        public List<ApplicationUserInfo> ReadAllEngineersManagers() 
        {
            List<ApplicationUserInfo> engManUsers = new List<ApplicationUserInfo>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                //read all where engineers and managers(ie not Accountant roles (AspNetRoles)) from AspNetUsers
                string query = @"select AspNetUsers.Name,ResponsibleDistrict,ApprovableLabour,ApprovableCost,UserName,AspNetRoles.Name as UserType from AspNetUsers 
                                left join AspNetUserRoles on AspNetUsers.Id=AspNetUserRoles.UserId
								left join AspNetRoles on AspNetUserRoles.RoleId=AspNetRoles.Id
                                where AspNetUserRoles.RoleId=1 or AspNetUserRoles.RoleId=2;";

                SqlCommand command = new SqlCommand(query, conn);

                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        engManUsers.Add(userInfoBuilder(result));
                    }
                }
            }
            return engManUsers;
        }


        public ApplicationUserInfo ReadUserByUserName(string userName)
        {
            if (userName == null) userName = "";
            ApplicationUserInfo user = new ApplicationUserInfo();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"select AspNetUsers.Name,ResponsibleDistrict,ApprovableLabour,ApprovableCost,UserName,AspNetRoles.Name as UserType from AspNetUsers 
                                    left join AspNetUserRoles on AspNetUsers.Id=AspNetUserRoles.UserId
                                    left join AspNetRoles on AspNetUserRoles.RoleId=AspNetRoles.Id
                                    where UserName = @userName";

                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("userName", userName));

                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        user = userInfoBuilder(result);
                    }
                }
            }
            return user;
        }


        //maybe UpdateRegion()
        public void changeDistrict(string userName, District district)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"UPDATE AspNetUsers 
                                SET ResponsibleDistrict = @district   
                                WHERE UserName = @userName";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("district", district));
                command.Parameters.Add(new SqlParameter("userName", userName));
                command.ExecuteNonQuery();
                conn.Close();
            }
        }

    }
}
