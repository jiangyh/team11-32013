﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnterpriseAssignment.Models;
using System.Configuration;

namespace EnterpriseAssignment.DataAccess
{
    public class ClientDataAccess
    {

        private string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public void Create(Client client)
        {
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"INSERT INTO Client VALUES (
                                @Name, 
                                @Type, 
                                @Location, 
                                @DistrictName, 
                                @Contact)";

                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("Name", client.Name));
                command.Parameters.Add(new SqlParameter("Type", client.Type.ToString()));
                command.Parameters.Add(new SqlParameter("Location", client.Location));
                command.Parameters.Add(new SqlParameter("DistrictName", client.DistrictName.ToString()));
                command.Parameters.Add(new SqlParameter("Contact", client.Contact));
                command.ExecuteNonQuery();

                conn.Close();
            }
        }

        public List<Client> ReadAllClients()
        {
            List<Client> clients = new List<Client>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"select * from Client";
                SqlCommand command = new SqlCommand(query, conn);
                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        clients.Add(clientBuilder(result));
                    }
                }
            }
            return clients;
        }

        public List<Client> ReadDistrictClients(District district)
        {
            List<Client> clients = new List<Client>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();

                string query = @"select * from Client where DistrictName = @districtName";
                SqlCommand command = new SqlCommand(query, conn);
                command.Parameters.Add(new SqlParameter("districtName",district.ToString()));

                using (SqlDataReader result = command.ExecuteReader())
                {
                    while (result.Read())
                    {
                        clients.Add(clientBuilder(result));
                    }
                }
            }
            return clients;
        }

        private Client clientBuilder(SqlDataReader result)
        {
            int clientId = result.GetInt32(0);
            string name = result.GetString(1);
            ClientType type;
            Enum.TryParse(result.GetString(2), true, out type);
            string location = result.GetString(3);
            District districtName;
            Enum.TryParse(result.GetString(4), true, out districtName);
            string contact = result.GetString(5);

            return new Client(clientId, name, type, location, districtName, contact);

           

        }
    }
}
